//
//  MCTestViewController.swift
//  SessionShare Test
//
//  Created by Alessandro Scala on 06/12/2018.
//  Copyright © 2018 Alessandro Scala. All rights reserved.
//

import UIKit

class MCTestViewController: UIViewController {

    var session: MultipeerSession!
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        session = MultipeerSession()
    }
    
    var text: String?{
        get{
            return _text
        }
        set(value){
            print(value)
            _text = value
        }
    }

    var _text: String?
    
    @IBAction func onUpdate(_ sender: Any) {
        label.text = _text
    }
    
    @IBAction func sendNames(_ sender: Any) {
        if session.connectedPeers.count > 0{
            session.sendToAllPeers(Data(count: 2))
        }
    }
}

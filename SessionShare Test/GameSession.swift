//
//  GameSession.swift
//  SessionShare Test
//
//  Created by Alessandro Scala on 06/12/2018.
//  Copyright © 2018 Alessandro Scala. All rights reserved.
//

import MultipeerConnectivity

class GameSession: NSObject {
    
    static let serviceType = "tls-yarn"
    
    private var peerID = MCPeerID(displayName: UIDevice.current.name)
    private var session: MCSession!
    private var mcAdvertiser: MCNearbyServiceAdvertiser!
    private var mcBrowser: MCNearbyServiceBrowser!
    public var delegate: NetworkDelegate?
    
    private var receivedDataHandler: (Data, MCPeerID) -> Void
    
    public var connectedPeers: [MCPeerID]{
        get{
            return session.connectedPeers
        }
    }
    
    init(receivedDataHandler: @escaping (Data, MCPeerID) -> Void){
        self.receivedDataHandler = receivedDataHandler
        super.init()
        
        session = MCSession(peer: peerID, securityIdentity: nil, encryptionPreference: .required)
        session.delegate = self
        
        mcAdvertiser = MCNearbyServiceAdvertiser(peer: peerID, discoveryInfo: nil, serviceType: GameSession.serviceType)
        mcAdvertiser.delegate = self
        mcAdvertiser.startAdvertisingPeer()
        
        mcBrowser = MCNearbyServiceBrowser(peer: peerID, serviceType: GameSession.serviceType)
        mcBrowser.delegate = self
        mcBrowser.startBrowsingForPeers()
    }
    
    func sendToPeers(data: Data){
        do{
            try session.send(data, toPeers: connectedPeers, with: .reliable)
        }catch{
            fatalError("Failed to send data to peers :\(error.localizedDescription)")
        }
    }
}

extension GameSession: MCSessionDelegate{
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        //Do nothing
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        receivedDataHandler(data, peerID)
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        //Do nothing
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        //Do nothing
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        //Do nothing
    }
}

extension GameSession: MCNearbyServiceAdvertiserDelegate{
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        invitationHandler(true, self.session)
    }
}

extension GameSession: MCNearbyServiceBrowserDelegate{
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        browser.invitePeer(peerID, to: session, withContext: nil, timeout: 10)
        print(peerID.displayName + " found")
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        //Do nothing
    }
}


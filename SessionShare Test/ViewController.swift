//
//  ViewController.swift
//  SessionShare Test
//
//  Created by Alessandro Scala on 06/12/2018.
//  Copyright © 2018 Alessandro Scala. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import MultipeerConnectivity

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    var session: MultipeerSession!
    var yarnPlaced: Bool = false
    
    var yarnNode: SCNNode!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        session = MultipeerSession()
        session.delegate = self
        
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints, .showBoundingBoxes]
        UIApplication.shared.isIdleTimerDisabled = true
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
        // Create a new scene
        let scene = SCNScene(named: "art.scnassets/Yarn Model0.1/yARn.scn")!
        
        yarnNode = scene.rootNode.childNode(withName: "yARn", recursively: true)!
        yarnNode.scale = SCNVector3(x: 0.1, y: 0.1, z: 0.1)
        // Set the scene to the view
        sceneView.scene = scene
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()

        configuration.planeDetection = .horizontal
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }

    // MARK: - ARSCNViewDelegate
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        if let name = anchor.name, name.hasPrefix("Yarn"){
            node.addChildNode(yarnNode)
        }
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
    
    @IBAction func onTap(_ sender: UITapGestureRecognizer) {
        print("Tapped")
        guard let hit = sceneView.hitTest(sender.location(in: sceneView), types: [.existingPlaneUsingGeometry, .estimatedHorizontalPlane]).first else { return }
        
        if !yarnPlaced{
            let anchor = ARAnchor(name: "Yarn", transform: hit.worldTransform)
        
            sceneView.session.add(anchor: anchor)
        
            yarnPlaced = true
        
            //let data = try NSKeyedArchiver.archivedData(withRootObject: anchor, requiringSecureCoding: true)
            print("Sending yarn anchor")
            session.send(yarnPosition: anchor)
        }else{
            var startPos = /*sceneView.session.currentFrame!.camera.transform*/ sceneView.pointOfView!.simdWorldTransform
            startPos.columns.3.x += 0.03
            startPos.columns.3.z -= 0.03
            let destPos = /*SCNVector3(x: Float(sender.location(in: sceneView).x), y: Float(sender.location(in: sceneView).y), z: -1)*/ hit.worldTransform
            let cookie = CookieThrow(position: startPos, direction: destPos)
            session.send(cookieThrow: cookie)
            throwCookie(action: cookie)
        }
    }
    
    func receivedData(data: Data, peerID: MCPeerID){
        do{
            if let anchor = try NSKeyedUnarchiver.unarchivedObject(ofClass: ARAnchor.self, from: data){
                print("Got anchor from \(peerID.displayName)")
                sceneView.session.add(anchor: anchor)
            } else {
                print("Got unknown object from \(peerID.displayName)")
                return
            }
        }catch{
            return
        }
    }
    
    
    @IBAction func sendWorld(_ sender: Any) {
        sceneView.session.getCurrentWorldMap(completionHandler: { (map, err) in
            guard map != nil else { fatalError(err?.localizedDescription ?? "Error") }
            self.session.send(worldMap: map!)
            print("World sent")
        })
    }
    
    
//    @IBAction func onDrag(_ sender: UIPanGestureRecognizer) {
//        for i in 0..<sender.numberOfTouches{
//            let pos = sender.location(ofTouch: i, in: sceneView)
//            sceneView.session.currentFrame?.camera.transform
//        }
//    }
}

extension ViewController: NetworkDelegate{
    
    func setWorld(map: ARWorldMap) {
        let conf = ARWorldTrackingConfiguration()
        conf.planeDetection = .horizontal
        conf.initialWorldMap = map
        sceneView.session.run(conf)
    }
    
    func throwCookie(action: CookieThrow){
        let geometry = SCNSphere(radius: 0.1)
        let node = SCNNode(geometry: geometry)
        let physicsBody = SCNPhysicsBody(type: .dynamic, shape: SCNPhysicsShape(geometry: geometry, options: nil))
        physicsBody.isAffectedByGravity = false
        physicsBody.mass = 0.1
        sceneView.scene.rootNode.addChildNode(node)
        node.position = action.position.translation
        node.look(at: action.direction.translation)
        print(node.worldPosition)
        print(sceneView.pointOfView!.worldPosition)
        physicsBody.resetTransform()
        physicsBody.applyForce(node.convertVector(SCNVector3(0,0,-action.velocity), to: sceneView.scene.rootNode), asImpulse: true)
        node.physicsBody = physicsBody
    }
    
    func setYarnPosition(position: ARAnchor) {
        sceneView.session.add(anchor: position)
        yarnPlaced = true
    }
}

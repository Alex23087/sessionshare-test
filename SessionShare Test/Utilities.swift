//
//  Utilities.swift
//  SessionShare Test
//
//  Created by Alessandro Scala on 09/12/2018.
//  Copyright © 2018 Alessandro Scala. All rights reserved.
//

import Foundation
import SceneKit

extension SCNVector3{
    static func *(rhs: SCNVector3, lhs: Float) -> SCNVector3{
        return SCNVector3(rhs.x * lhs, rhs.y * lhs, rhs.x * lhs)
    }
    static func *(rhs: Float, lhs: SCNVector3) -> SCNVector3{
        return SCNVector3(lhs.x * rhs, lhs.y * rhs, lhs.x * rhs)
    }
}

extension float4x4{
    var translation: SCNVector3{
        return SCNVector3(x: columns.3.x, y: columns.3.y, z: columns.3.z)
    }
}

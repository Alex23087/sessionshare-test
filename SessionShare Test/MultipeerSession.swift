//
//  MultipeerSession.swift
//  SessionShare Test
//
//  Created by Alessandro Scala on 06/12/2018.
//  Copyright © 2018 Alessandro Scala. All rights reserved.
//

import MultipeerConnectivity
import ARKit

class MultipeerSession: NSObject {
    static let serviceType = "tls-yarn"
    
    private let myPeerID = MCPeerID(displayName: UIDevice.current.name)
    private var session: MCSession!
    private var serviceAdvertiser: MCNearbyServiceAdvertiser!
    private var serviceBrowser: MCNearbyServiceBrowser!
    private var dataQueue: [String: NetAction]!
    private var state: State = .undefined
    public var delegate: NetworkDelegate?
    
    var connectedPeers: [MCPeerID] {
        return session.connectedPeers
    }
    
    enum NetAction: Int, Codable{
        case sendWorld
        case throwCookie
        case setYarnPosition
    }
    
    enum State{
        case firstJoin
        case join
        case connected
        case firstHost
        case undefined
    }
    
    init(with state: State = .undefined) {
        super.init()
        
        self.state = state
        
        session = MCSession(peer: myPeerID, securityIdentity: nil, encryptionPreference: .required)
        session.delegate = self
        
        serviceAdvertiser = MCNearbyServiceAdvertiser(peer: myPeerID, discoveryInfo: nil, serviceType: MultipeerSession.serviceType)
        serviceAdvertiser.delegate = self
        serviceAdvertiser.startAdvertisingPeer()
        
        serviceBrowser = MCNearbyServiceBrowser(peer: myPeerID, serviceType: MultipeerSession.serviceType)
        serviceBrowser.delegate = self
        serviceBrowser.startBrowsingForPeers()
        
        if state == State.firstJoin{
            delegate?.startBrowsing()
        }
        
        dataQueue = [:]
    }
    
    func sendToAllPeers(_ data: Data) {
        do {
            try session.send(data, toPeers: connectedPeers, with: .reliable)
        } catch {
            print("error sending data to peers: \(error.localizedDescription)")
        }
    }
}

extension MultipeerSession: MCSessionDelegate {
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        delegate?.peerCountChanged(count: self.connectedPeers.count)
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        if let action = dataQueue[peerID.displayName]{
            switch action{
            case .sendWorld:
                guard let world = try! NSKeyedUnarchiver.unarchivedObject(ofClass: ARWorldMap.self, from: data) else {fatalError("Error unpacking world")}
                print("Setting World")
                delegate?.setWorld(map: world)
            case .throwCookie:
                guard let cookie = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as! CookieThrow else {
                    print("Error unpacking cookie throw data")
                    dataQueue[peerID.displayName] = nil
                    return
                }
                print("Throwing cookie")
                delegate?.throwCookie(action: cookie)
            case .setYarnPosition:
                guard let anchor = try! NSKeyedUnarchiver.unarchivedObject(ofClass: ARAnchor.self, from: data) else {fatalError("Failed unpacking Yarn anchor data")}
                print("Setting Yarn position")
                delegate?.setYarnPosition(position: anchor)
            }
            dataQueue[peerID.displayName] = nil
        }else{
            do{
                guard let action = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) else { print("Error getting peer action"); return }
                dataQueue[peerID.displayName] = NetAction(rawValue: action as! Int)
                print(dataQueue)
            }catch{
                print(error.localizedDescription)
            }
        }
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        fatalError("This service does not send/receive streams.")
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        fatalError("This service does not send/receive resources.")
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        fatalError("This service does not send/receive resources.")
    }
    
}

extension MultipeerSession: MCNearbyServiceBrowserDelegate {
    
    public func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String: String]?) {
        if state == State.firstJoin{
            delegate?.foundPeer(peerID: peerID)
        }
        if delegate?.shouldInvite(peer: peerID) ?? false{
            browser.invitePeer(peerID, to: session, withContext: nil, timeout: 10)
            print("Invited peer \(peerID.displayName)")
        }
    }
    
    public func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        if state == State.firstJoin{
            delegate?.lostPeer(peerID: peerID)
        }
    }
    
}

extension MultipeerSession: MCNearbyServiceAdvertiserDelegate {
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        delegate?.connectionRequest(from: peerID, to: self.session){accept, session in
            if accept{
                print("Accepting invitation from \(peerID.displayName)")
                self.delegate?.peerCountChanged(count: self.connectedPeers.count+1)
            }else{
                print("Declining invitation from \(peerID.displayName)")
            }
            invitationHandler(accept, session)
        }
    }
    
}

extension MultipeerSession{     //Actions
    func send(worldMap: ARWorldMap){
        guard let data = try? NSKeyedArchiver.archivedData(withRootObject: worldMap, requiringSecureCoding: true) else {
            fatalError("Fatal error encoding world")
        }
        sendToAllPeers(try! NSKeyedArchiver.archivedData(withRootObject: NetAction.sendWorld.rawValue, requiringSecureCoding: false))
        sendToAllPeers(data)
    }
    
    func send(cookieThrow: CookieThrow){
        do{
            sendToAllPeers(try NSKeyedArchiver.archivedData(withRootObject: NetAction.throwCookie.rawValue, requiringSecureCoding: false))
            sendToAllPeers(try NSKeyedArchiver.archivedData(withRootObject: cookieThrow, requiringSecureCoding: false))
        }catch{
            print(error.localizedDescription)
            fatalError()
        }
    }
    
    func send(yarnPosition: ARAnchor){
        do{
            sendToAllPeers(try NSKeyedArchiver.archivedData(withRootObject: NetAction.setYarnPosition.rawValue, requiringSecureCoding: false))
            sendToAllPeers(try NSKeyedArchiver.archivedData(withRootObject: yarnPosition, requiringSecureCoding: true))
        }catch{
            fatalError("Error sending Yarn position\n\(error.localizedDescription)")
        }
    }
}


protocol NetworkDelegate{
    func setWorld(map: ARWorldMap)
    func throwCookie(action: CookieThrow)
    func setYarnPosition(position: ARAnchor)
    func connectionRequest(from peer: MCPeerID, to session: MCSession, handler: @escaping (Bool, MCSession) -> Void)
    func shouldInvite(peer: MCPeerID) -> Bool
    func peerCountChanged(count: Int)
    func foundPeer(peerID: MCPeerID)
    func lostPeer(peerID: MCPeerID)
    func startBrowsing()
}

extension NetworkDelegate{  //Optional methods by default implementation
    func connectionRequest(from peer: MCPeerID, to session: MCSession, handler: @escaping (Bool, MCSession) -> Void) {handler(true,session)}
    func shouldInvite(peer: MCPeerID) -> Bool {return true}
    func peerCountChanged(count: Int) {}
    func foundPeer(peerID: MCPeerID) {}
    func lostPeer(peerID: MCPeerID) {}
    func startBrowsing() {}
}

/*  A default UI to select a peer to connect to the first time you are joining a game
extension NetworkDelegate where Self : UIViewController{
    private var _dialog: UIAlertController?{
        get{
            return __dialog
        }
        set(value){
            __dialog = value
        }
    }
    
    func startBrowsing(){
        __dialog = UIAlertController(title: "Join", message: "Tap a name to join", preferredStyle: .alert)
        //Create a tableView with the peers here
        present(_dialog!, animated: true, completion: nil)
    }
    
    func foundPeer(peerID: MCPeerID){
        
    }
}

fileprivate var __dialog: UIAlertController?*/
